ext {

    // Android
    minSdkVersion = 23
    targetSdkVersion = 29
    compileSdkVersion = 29
    appId = "com.lupinixio.omnifidemo"
    versionCode = 1
    versionName = "1.0"
    testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    jvmTarget = "1.8"

    //Dependency versions
    def gradle_version = '3.5.2'
    def google_services_version = '4.3.2'
    def kotlin_version = '1.3.50'
    def retrofit_version = '2.6.2'
    def androidx_core_ktx_version = '1.2.0-alpha04'
    def rx_kotlin_version = '2.2.0'
    def rx_android_version = '2.1.1'
    def lifecycle_version = '2.2.0-alpha05'
    def nav_version = '2.2.0-alpha03'
    def firebase_analytics_version = '17.1.0'
    def firebase_messaging_version = '20.0.0'
    def firebase_core_version = '17.1.0'
    def firabase_crashlytics_version = '2.10.1'
    def gson_version = '2.8.6'
    def room_version = '2.1.0'
    def junit_version = '4.12'
    def dagger_version = '2.14.1'
    def picasso_version = '2.71828'
    def koin_version = '2.0.1'
    def circular_image_view_version = '3.0.1'
    def mockito_version = '1.10.19'
    def constraint_layout_version = '2.0.0-beta2'
    def app_compat_version = '1.1.0'
    def google_map_version = '17.0.0'

    //ClassPath
    classPathDependencies = [
            gradle        : "com.android.tools.build:gradle:${gradle_version}",
            kotlin        : "org.jetbrains.kotlin:kotlin-gradle-plugin:${kotlin_version}",
            navigation    : "androidx.navigation:navigation-safe-args-gradle-plugin:${nav_version}",
            googleServices: "com.google.gms:google-services:${google_services_version}"
    ]

    //Kotlin
    kotlinDependencies = [
            kotlinJDK8     : "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${kotlin_version}",
            andoridxCoreKtx: "androidx.core:core-ktx:${androidx_core_ktx_version}"
    ]

    //Android
    androidDependencies = [
            appCompat: "androidx.appcompat:appcompat:${app_compat_version}"
    ]

    //RxKotlin
    rxKotlinDependencies = [
            rxKotlin : "io.reactivex.rxjava2:rxkotlin:${rx_kotlin_version}",
            rxAndroid: "io.reactivex.rxjava2:rxandroid:${rx_android_version}",
            rxBinding: "com.jakewharton.rxbinding2:rxbinding-appcompat-v7:${rx_android_version}"
    ]

    //Retrofit
    retrofitDependencies = [
            retrofit             : "com.squareup.retrofit2:retrofit:${retrofit_version}",
            retrofitGsonConverter: "com.squareup.retrofit2:converter-gson:${retrofit_version}",
            retrofitRxAdapter    : "com.squareup.retrofit2:adapter-rxjava2:${retrofit_version}"
    ]

    // ViewModel and LiveData
    viewmodelAndLiveDataDependencies = [
            lifecycleExtension: "androidx.lifecycle:lifecycle-extensions:${lifecycle_version}"
    ]

    //Navigation
    navigationDependencies = [
            navigationFragment: "androidx.navigation:navigation-fragment-ktx:${nav_version}",
            navigationUI      : "androidx.navigation:navigation-ui-ktx:${nav_version}"
    ]

    //Firebase
    firebaseDependencies = [
            firebaseAnalytics  : "com.google.firebase:firebase-analytics:${firebase_analytics_version}",
            firebaseMessaging  : "com.google.firebase:firebase-messaging:${firebase_messaging_version}",
            firebaseCore       : "com.google.firebase:firebase-core:${firebase_core_version}",
            firebaseCrashlytics: "com.crashlytics.sdk.android:crashlytics:${firabase_crashlytics_version}"
    ]

    //Gson
    gson = "com.google.code.gson:gson:$gson_version"

    //Room
    roomDependencies = [
            roomRuntime: "androidx.room:room-runtime:${room_version}",
            roomRxJava2: "androidx.room:room-rxjava2:${room_version}"
    ]

    //Koin
    koinDependencies = [
            koinAndroid  : "org.koin:koin-android:${koin_version}",
            koinViewModel: "org.koin:koin-androidx-viewmodel:${koin_version}"
    ]

    // Testing
    testingDependencies = [
            junit            : "junit:junit:${junit_version}",
            androidTestRunner: 'androidx.test:runner:1.1.0-alpha4',
            roomTest         : "androidx.room:room-testing:${room_version}",
            mockito          : "org.mockito:mockito-core:${mockito_version}"
    ]

    //Layout
    layoutDependencies = [
            recyclerView     : 'androidx.recyclerview:recyclerview:1.0.0',
            constraintLayout : "androidx.constraintlayout:constraintlayout:${constraint_layout_version}",
            picasso          : "com.squareup.picasso:picasso:${picasso_version}",
            circularImageView: "de.hdodenhof:circleimageview:${circular_image_view_version}"
    ]

    compilerDependencies = [
            roomCompiler     : "androidx.room:room-compiler:${room_version}",
            lifecycleCompiler: "androidx.lifecycle:lifecycle-compiler:${lifecycle_version}"
    ]

    googleMapDependencies = [
            playServicesMaps: "com.google.android.gms:play-services-maps:${google_map_version}"
    ]


}