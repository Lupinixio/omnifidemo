package com.lupinixio.omnifidemo.persistence.dao

import androidx.room.Dao
import androidx.room.Query
import com.lupinixio.omnifidemo.persistence.entities.RestaurantEntity
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface RestaurantDao : BaseDao<RestaurantEntity> {

    @Query("SELECT * FROM restaurants")
    fun getAllRestaurants(): Maybe<List<RestaurantEntity>>

    @Query("SELECT * FROM restaurants WHERE name LIKE :name AND latitude LIKE :latitude AND longitude LIKE :longitude")
    fun getRestaurantByName(
        name: String,
        latitude: Double,
        longitude: Double
    ): Single<RestaurantEntity>
}