package com.lupinixio.omnifidemo.persistence

import android.app.Application
import androidx.room.Room
import com.lupinixio.omnifidemo.app.IModulesProvider
import com.lupinixio.omnifidemo.domain.RestaurantPersistencePort
import com.lupinixio.omnifidemo.persistence.DatabaseMetaData.NAME
import org.koin.dsl.module


object PersistenceModule : IModulesProvider {

    private val databaseModules = module {
        single {
            val app: Application = get()
            Room.databaseBuilder(app, OmnifiDatabase::class.java, NAME).build()
        }
    }

    private val daoModules = module {
        single { get<OmnifiDatabase>().restaurantDao() }
    }

    private val portsModules = module {
        single<RestaurantPersistencePort> { RestaurantPersistenceAdapter(get()) }
    }

    override val modules = listOf(databaseModules, daoModules, portsModules)
}