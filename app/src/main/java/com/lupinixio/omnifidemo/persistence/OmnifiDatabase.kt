package com.lupinixio.omnifidemo.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lupinixio.omnifidemo.persistence.dao.RestaurantDao
import com.lupinixio.omnifidemo.persistence.entities.RestaurantEntity

object DatabaseMetaData {
    const val NAME = "omnifi.db"
    const val VERSION = 1
}

@Database(entities = [RestaurantEntity::class], version = DatabaseMetaData.VERSION)
abstract class OmnifiDatabase : RoomDatabase() {

    abstract fun restaurantDao(): RestaurantDao
}