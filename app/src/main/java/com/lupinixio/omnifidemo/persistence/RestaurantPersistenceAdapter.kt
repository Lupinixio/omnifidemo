package com.lupinixio.omnifidemo.persistence

import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import com.lupinixio.omnifidemo.domain.RestaurantPersistencePort
import com.lupinixio.omnifidemo.persistence.dao.RestaurantDao
import com.lupinixio.omnifidemo.persistence.entities.RestaurantEntity
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class RestaurantPersistenceAdapter(private val restaurantDao: RestaurantDao) :
    RestaurantPersistencePort {

    override fun saveRestaurants(restaurants: List<Restaurant>): Single<List<Long>> {
        return restaurantDao.insertAll(restaurants.map {
            RestaurantEntity(
                it.name,
                it.position.latitude,
                it.position.longitude,
                it.body,
                it.deliveryLink
            )
        }).subscribeOn(Schedulers.computation())
    }

    override fun getRestaurants(): Maybe<List<Restaurant>> {
        return restaurantDao.getAllRestaurants()
            .map { it.map { item -> item as Restaurant } }
            .subscribeOn(Schedulers.computation())
    }

    override fun getRestaurant(
        name: String,
        latitude: Double,
        longitude: Double
    ): Single<Restaurant> {
        return restaurantDao.getRestaurantByName(name, latitude, longitude)
            .map { it as Restaurant }
            .subscribeOn(Schedulers.computation())
    }
}