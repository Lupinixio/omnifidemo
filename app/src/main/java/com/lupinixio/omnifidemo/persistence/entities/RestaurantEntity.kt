package com.lupinixio.omnifidemo.persistence.entities

import androidx.room.Entity
import com.google.android.gms.maps.model.LatLng
import com.lupinixio.omnifidemo.abstraction.dto.Restaurant

@Entity(tableName = "restaurants", primaryKeys = ["name", "latitude", "longitude"])
data class RestaurantEntity(
    override val name: String,
    val latitude: Double,
    val longitude: Double,
    override val body: String,
    override val deliveryLink: String
) : Restaurant {
    override val position: LatLng
        get() = LatLng(latitude, longitude)
}