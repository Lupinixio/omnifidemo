package com.lupinixio.omnifidemo.abstraction.usecase

import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import io.reactivex.Maybe
import io.reactivex.Observable

interface RestaurantUseCase {

    fun getRestaurants(): Observable<List<Restaurant>>

    fun getRestaurant(
        restaurantName: String, restaurantLatitude: Double,
        restaurantLongitude: Double
    ): Observable<Restaurant>
}