package com.lupinixio.omnifidemo.abstraction.dto

import com.google.android.gms.maps.model.LatLng

interface Restaurant {
    val name: String
    val position: LatLng
    val body: String
    val deliveryLink: String
}