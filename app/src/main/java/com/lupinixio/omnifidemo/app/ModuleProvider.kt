package com.lupinixio.omnifidemo.app

import android.content.ContentResolver
import com.lupinixio.omnifidemo.domain.DomainModule
import com.lupinixio.omnifidemo.network.NetworkModule
import com.lupinixio.omnifidemo.persistence.PersistenceModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

object ModuleProvider {

    private val appModules = module {
        single<ContentResolver> { androidContext().contentResolver }
    }

    val modules: List<Module>
        get() {
            return ArrayList<Module>().apply {
                add(appModules)
                addAll(DomainModule.modules)
                addAll(NetworkModule.modules)
                addAll(PersistenceModule.modules)
            }
        }
}