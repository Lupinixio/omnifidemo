package com.lupinixio.omnifidemo.app

import org.koin.core.module.Module

interface IModulesProvider {

    val modules: List<Module>
}