package com.lupinixio.omnifidemo.app

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class OmnifiApplication :Application(){

    override fun onCreate() {
        super.onCreate()
        startKoin {
            printLogger()
            androidContext(this@OmnifiApplication)
            modules(ModuleProvider.modules)
        }
    }
}