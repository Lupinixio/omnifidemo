package com.lupinixio.omnifidemo.domain

import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

interface RestaurantPersistencePort {

    fun saveRestaurants(restaurants: List<Restaurant>): Single<List<Long>>

    fun getRestaurants(): Maybe<List<Restaurant>>

    fun getRestaurant(name: String, latitude: Double, longitude: Double): Single<Restaurant>
}