package com.lupinixio.omnifidemo.domain

import com.lupinixio.omnifidemo.abstraction.usecase.RestaurantUseCase
import com.lupinixio.omnifidemo.app.IModulesProvider
import org.koin.core.module.Module
import org.koin.dsl.module


object DomainModule : IModulesProvider {

    private val useCaseModules = module {
        single { RestaurantRepositoryUseCase(get(), get()) as RestaurantUseCase }
    }

    override val modules: List<Module>
        get() = listOf(useCaseModules)

}