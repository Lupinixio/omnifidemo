package com.lupinixio.omnifidemo.domain

import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import com.lupinixio.omnifidemo.abstraction.usecase.RestaurantUseCase
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable

class RestaurantRepositoryUseCase(
    private val restaurantNetwork: RestaurantNetworkPort,
    private val restaurantPersistence: RestaurantPersistencePort
) :
    RestaurantUseCase {


    override fun getRestaurants(): Observable<List<Restaurant>> {
        return Observable.concat(
            restaurantPersistence.getRestaurants().toObservable(),
            restaurantNetwork.getRestaurants().doOnNext {
                restaurantPersistence.saveRestaurants(it)
                    .ignoreElement()
                    .onErrorComplete()
                    .subscribe()
            }
        )
    }

    override fun getRestaurant(
        restaurantName: String, restaurantLatitude: Double,
        restaurantLongitude: Double
    ): Observable<Restaurant> {
        return restaurantPersistence.getRestaurant(
            restaurantName,
            restaurantLatitude,
            restaurantLongitude
        ).toObservable()
    }
}