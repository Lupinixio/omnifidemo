package com.lupinixio.omnifidemo.domain

import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import io.reactivex.Observable

interface RestaurantNetworkPort {

    fun getRestaurants(): Observable<List<Restaurant>>
}