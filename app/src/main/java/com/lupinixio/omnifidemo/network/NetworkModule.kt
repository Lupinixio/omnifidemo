package com.lupinixio.omnifidemo.network

import com.lupinixio.omnifidemo.app.IModulesProvider
import com.lupinixio.omnifidemo.domain.RestaurantNetworkPort
import org.koin.core.module.Module
import org.koin.dsl.module


object NetworkModule : IModulesProvider {


    private val apiModules = module {
        single { OmnifiApi as OmnifiApiClient }
    }

    private val portModules = module {
        single { RestaurantApiAdapter(get()) as RestaurantNetworkPort }
    }

    override val modules: List<Module>
        get() = listOf(apiModules, portModules)
}