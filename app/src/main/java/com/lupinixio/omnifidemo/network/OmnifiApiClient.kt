package com.lupinixio.omnifidemo.network

import com.lupinixio.omnifidemo.network.api.RestaurantAPI
import io.reactivex.Single
import retrofit2.http.GET

interface OmnifiApiClient {

    @GET("trg_restaurant_feed/JSON")
    fun getRestaurants(): Single<List<RestaurantAPI>>
}