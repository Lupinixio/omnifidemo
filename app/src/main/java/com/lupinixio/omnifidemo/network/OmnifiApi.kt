package com.lupinixio.omnifidemo.network

import com.lupinixio.omnifidemo.network.api.RestaurantAPI
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object OmnifiApi : OmnifiApiClient {

    private val TAG = javaClass.simpleName
    private const val base_url = "https://www.frankieandbennys.com/"

    private val onHttpClient: OkHttpClient by lazy { OkHttpClient().newBuilder().build() }
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(base_url)
            .client(onHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private val client = retrofit.create(OmnifiApiClient::class.java)

    override fun getRestaurants(): Single<List<RestaurantAPI>> {
        return client.getRestaurants()
    }
}