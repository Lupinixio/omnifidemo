package com.lupinixio.omnifidemo.network.api

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import com.lupinixio.omnifidemo.abstraction.dto.Restaurant

data class RestaurantAPI(
    @SerializedName("country")
    val country: String = "",
    @SerializedName("city")
    val city: String = "",
    @SerializedName("open_thursday")
    val openThursday: String = "",
    @SerializedName("additional")
    val additional: String = "",
    @SerializedName("latitude")
    val latitude: String = "",
    @SerializedName("open_wednesday")
    val openWednesday: String = "",
    @SerializedName("delivery_link")
    override val deliveryLink: String = "",
    @SerializedName("taxonomy")
    val taxonomy: String = "",
    @SerializedName("title")
    val title: String = "",
    @SerializedName("body")
    override val body: String = "",
    @SerializedName("booking_id")
    val bookingId: String = "",
    @SerializedName("street")
    val street: String = "",
    @SerializedName("uberEatsDirectLink")
    val uberEatsDirectLink: String = "",
    @SerializedName("enableOrderAhead")
    val enableOrderAhead: String = "",
    @SerializedName("fax")
    val fax: String = "",
    @SerializedName("open_tuesday")
    val openTuesday: String = "",
    @SerializedName("longitude")
    val longitude: String = "",
    @SerializedName("ccVersion")
    val ccVersion: String = "",
    @SerializedName("voucherBand")
    val voucherBand: String = "",
    @SerializedName("salesAreaId")
    val salesAreaId: String = "",
    @SerializedName("clickAndCollectDiscountCopy")
    val clickAndCollectDiscountCopy: String = "",
    @SerializedName("open_monday")
    val openMonday: String = "",
    @SerializedName("telephone")
    val telephone: String = "",
    @SerializedName("displayInSiteDirectory")
    val displayInSiteDirectory: Boolean = false,
    @SerializedName("url")
    val url: String = "",
    @SerializedName("payMyBillVersion")
    val payMyBillVersion: String = "",
    @SerializedName("enablePayMyBill")
    val enablePayMyBill: String = "",
    @SerializedName("open_friday")
    val openFriday: String = "",
    @SerializedName("zonalSiteId")
    val zonalSiteId: String = "",
    @SerializedName("name")
    override val name: String = "",
    @SerializedName("open_sunday")
    val openSunday: String = "",
    @SerializedName("site_id")
    val siteId: String = "",
    @SerializedName("open_saturday")
    val openSaturday: String = "",
    @SerializedName("postal_code")
    val postalCode: String = "",
    @SerializedName("region")
    val region: String = "",
    @SerializedName("clickAndCollect")
    val clickAndCollect: Boolean = false
) : Restaurant {

    override val position: LatLng
        get() = LatLng(latitude.toDouble(), longitude.toDouble())
}