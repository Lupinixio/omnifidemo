package com.lupinixio.omnifidemo.network

import android.util.Log
import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import com.lupinixio.omnifidemo.domain.RestaurantNetworkPort
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class RestaurantApiAdapter(private val restaurantClient: OmnifiApiClient) : RestaurantNetworkPort {

    override fun getRestaurants(): Observable<List<Restaurant>> {
        return restaurantClient.getRestaurants()
            .map { it.map { item -> item as Restaurant } }
            .toObservable().subscribeOn(Schedulers.io())
    }

}