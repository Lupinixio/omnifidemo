package com.lupinixio.omnifidemo.presentation.base

import androidx.lifecycle.MutableLiveData

interface IViewModelStatus {
    val error: MutableLiveData<Throwable>
    val loadingInProgress: MutableLiveData<Boolean>
    val toastMessage: MutableLiveData<String>
}