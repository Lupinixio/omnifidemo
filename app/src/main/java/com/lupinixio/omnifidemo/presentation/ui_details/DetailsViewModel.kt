package com.lupinixio.omnifidemo.presentation.ui_details

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import com.lupinixio.omnifidemo.abstraction.usecase.RestaurantUseCase
import com.lupinixio.omnifidemo.presentation.base.BaseViewModel
import io.reactivex.rxkotlin.subscribeBy

class DetailsViewModel(
    private val restaurantUseCase: RestaurantUseCase,
    restaurantName: String,
    restaurantLatitude: Double,
    restaurantLongitude: Double
) :
    BaseViewModel() {

    private val restaurant = MutableLiveData<Restaurant>()
    private val deliveryLinkVisibility = MediatorLiveData<Int>().apply { value = View.GONE }

    init {
        loadRestaurantData(restaurantName, restaurantLatitude, restaurantLongitude)
        setupMediator()
    }

    val restaurantDetail: LiveData<Restaurant>
        get() = restaurant
    val linkViewVisibility: LiveData<Int>
        get() = deliveryLinkVisibility

    private fun setupMediator() {
        deliveryLinkVisibility.addSource(restaurant, onRestaurantChange())
    }

    private fun onRestaurantChange(): Observer<in Restaurant> {
        return Observer {
            restaurant.value?.let {
                deliveryLinkVisibility.value = if (it.deliveryLink.isNotEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }
        }
    }

    private fun loadRestaurantData(
        restaurantName: String,
        restaurantLatitude: Double,
        restaurantLongitude: Double
    ) {
        addDisposable(
            restaurantUseCase.getRestaurant(restaurantName, restaurantLatitude, restaurantLongitude)
                .subscribeBy(onNext = { restaurant.postValue(it) },
                    onError = { error.postValue(it) })
        )
    }


}