package com.lupinixio.omnifidemo.presentation.ui_details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.lupinixio.omnifidemo.R
import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import com.lupinixio.omnifidemo.databinding.FragmentDetailsBinding
import com.lupinixio.omnifidemo.presentation.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class DetailsFragment : BaseFragment<FragmentDetailsBinding>() {

    private val args: DetailsFragmentArgs by navArgs()
    private val vm: DetailsViewModel by viewModel {
        parametersOf(
            args.restaurantName,
            args.restaurantLatitude.toDouble(),
            args.restaurantLongitude.toDouble()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_details, container, false
        )
        binding.lifecycleOwner = this
        binding.model = vm
        return binding.root
    }

    override fun setupInitialView() {
        setVmCallback(vm)

    }

    override fun setupViewModelObservers() {
        vm.restaurantDetail.observe(this, onRestaurantChange())
    }

    private fun onRestaurantChange(): Observer<in Restaurant> {
        return Observer { restaurant ->

            binding.deliveryButton.setOnClickListener {
                val url = restaurant.deliveryLink
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }
    }
}