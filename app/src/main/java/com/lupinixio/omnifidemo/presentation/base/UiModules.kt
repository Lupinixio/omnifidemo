package com.lupinixio.omnifidemo.presentation.base

interface UiModules {
    fun load()
}