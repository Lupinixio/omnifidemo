package com.lupinixio.omnifidemo.presentation.activities

import com.lupinixio.omnifidemo.presentation.base.UiModules
import com.lupinixio.omnifidemo.presentation.ui_details.DetailsViewModel
import com.lupinixio.omnifidemo.presentation.ui_map.MapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.dsl.module


object MainActivityModules : UiModules {
    override fun load() = loadMainActivityModules

    private val loadMainActivityModules by lazy {
        loadKoinModules(
            listOf(
                viewModelModules
            )
        )
    }

    private val viewModelModules: Module = module {
        viewModel { MapViewModel(get()) }
        viewModel { (name: String, latitude:Double, longitude:Double) -> DetailsViewModel(get(), name, latitude, longitude) }
    }

}