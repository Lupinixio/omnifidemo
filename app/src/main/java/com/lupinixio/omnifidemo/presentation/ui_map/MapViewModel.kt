package com.lupinixio.omnifidemo.presentation.ui_map

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import com.lupinixio.omnifidemo.abstraction.usecase.RestaurantUseCase
import com.lupinixio.omnifidemo.presentation.base.BaseViewModel
import com.lupinixio.omnifidemo.presentation.ui_map.MapViewModelConfiguration.LONDON
import com.lupinixio.omnifidemo.presentation.ui_map.MapViewModelConfiguration.ZOOM_LEVEL
import io.reactivex.rxkotlin.subscribeBy

class MapViewModel(private val restaurantUseCase: RestaurantUseCase) : BaseViewModel() {

    private val downloadedList = MutableLiveData<List<Restaurant>>()
    private var cameraPosition = CameraPosition(LONDON, ZOOM_LEVEL, 0f, 0f)

    init {
        downloadRestaurantList()
    }

    val restaurantList: LiveData<List<Restaurant>>
        get() = downloadedList

    fun initializeMapView(googleMap: GoogleMap?) {
        googleMap ?: return

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    fun addMarker(googleMap: GoogleMap?) {
        googleMap ?: return

        googleMap.clear()
        Log.d(TAG, "Marker cleared")

        downloadedList.value?.let {
            for (restaurant in it) {
                googleMap.addMarker(MarkerOptions().apply {
                    position(restaurant.position)
                    title(restaurant.name)
                })
                Log.d(TAG, "Added marker for ${restaurant.name}")

            }
        }
    }

    fun saveCameraPosition(cp: CameraPosition) {
        cameraPosition = cp
    }

    private fun downloadRestaurantList() {
        addDisposable(
            restaurantUseCase.getRestaurants()
                .subscribeBy(onNext = { downloadedList.postValue(it) },
                    onError = { error.postValue(it) })
        )
    }


}

object MapViewModelConfiguration {
    val LONDON = LatLng(51.509865, -0.118092)
    val ZOOM_LEVEL = 13f
}
