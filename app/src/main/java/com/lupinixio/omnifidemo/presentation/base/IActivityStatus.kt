package com.lupinixio.omnifidemo.presentation.base

interface IActivityStatus {
    fun isLoading(isLoading: Boolean)
}