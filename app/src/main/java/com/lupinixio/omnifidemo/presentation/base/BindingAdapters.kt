package com.lupinixio.a20191014_cavalera_yooxdemo.ui_base

import android.os.Build
import android.text.Html
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter(value = ["app:text"])
fun TextView.setTextCustom(value: String?) {
    value ?: return

    text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(value, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(value)
    }
    movementMethod = LinkMovementMethod.getInstance()
}

