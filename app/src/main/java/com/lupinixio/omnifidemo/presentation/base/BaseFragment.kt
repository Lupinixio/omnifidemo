package com.lupinixio.omnifidemo.presentation.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.lupinixio.omnifidemo.R
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BaseFragment<B : ViewDataBinding> : Fragment() {

    protected val navController: NavController by lazy { findNavController() }
    private var toast: Toast? = null
    private var activityStatus: IActivityStatus? = null
    private var vmStatus: IViewModelStatus? = null
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    protected val TAG = javaClass.simpleName

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        setupViewModelObservers()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is IActivityStatus)
            activityStatus = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupInitialView()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    protected open fun initViewModel() {
        vmStatus?.error?.observe(this, onErrorChange())
        vmStatus?.loadingInProgress?.observe(this, onLoadingProgressChange())
        vmStatus?.toastMessage?.observe(this, onToastMessageChange())
    }

    protected fun setVmCallback(statusCallback: IViewModelStatus) {
        vmStatus = statusCallback
        initViewModel()
    }

    protected abstract fun setupInitialView()

    protected abstract fun setupViewModelObservers()

    protected open fun onLoadingProgressChange(): Observer<in Boolean> {
        return Observer {
            Log.d(TAG, "Loading progress set to -> $it")
            activityStatus?.isLoading(it)
        }
    }

    protected open fun onErrorChange(): Observer<in Throwable> {
        return Observer {
            Log.d(TAG, "Error message set to -> ${it.localizedMessage}")
            showToastMessage(it.localizedMessage ?: getString(R.string.generic_error))
        }
    }

    protected open fun onToastMessageChange(): Observer<in String> {
        return Observer {
            Log.d(TAG, "Toast message set to -> $it")
            showToastMessage(it)
        }
    }

    protected fun showToastMessage(message: String, duration: Int = Toast.LENGTH_LONG) {
        this.toast?.let {
            it.setText(message)
            it.duration = duration
            it.show()
        } ?: run {
            toast = Toast.makeText(context, message, duration)
            toast!!.show()
        }

    }

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    protected fun clearAllDisposable() {
        compositeDisposable.clear()
    }


}