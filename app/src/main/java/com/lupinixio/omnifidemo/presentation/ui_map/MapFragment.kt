package com.lupinixio.omnifidemo.presentation.ui_map

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Marker
import com.lupinixio.omnifidemo.R
import com.lupinixio.omnifidemo.abstraction.dto.Restaurant
import com.lupinixio.omnifidemo.databinding.FragmentMapBinding
import com.lupinixio.omnifidemo.presentation.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class MapFragment : BaseFragment<FragmentMapBinding>(), OnMapReadyCallback,
    GoogleMap.OnInfoWindowClickListener {

    private val vm: MapViewModel by viewModel()
    private var googleMap: GoogleMap? = null
    private var mapView: MapView? = null
    private val restaurantClick: (Marker) -> Unit =
        {
            navController.navigate(
                MapFragmentDirections.actionMapFragmentToDetailsFragment(
                    it.title,
                    it.position.latitude.toString(),
                    it.position.longitude.toString()
                )
            )
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_map, container, false
        )
        binding.lifecycleOwner = this

        mapView = binding.map
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync(this)

        return binding.root
    }


    override fun setupInitialView() {
        setVmCallback(vm)
    }

    override fun setupViewModelObservers() {
        vm.restaurantList.observe(this, onRestaurantListChange())
    }

    override fun onResume() {
        mapView?.onResume()
        super.onResume()
    }

    override fun onPause() {
        mapView?.onPause()
        googleMap?.let {
            vm.saveCameraPosition(it.cameraPosition)
        }
        super.onPause()
    }

    override fun onDestroy() {
        mapView?.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        mapView?.onLowMemory()
        super.onLowMemory()
    }

    private fun onRestaurantListChange(): Observer<List<Restaurant>> {
        return Observer {
            Log.d(TAG, "there are ${it.size} restaurants")
            vm.addMarker(googleMap)
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        Log.d(TAG, "map ready")
        googleMap = p0

        googleMap?.let {
            it.setInfoWindowAdapter(CustomInfoWindowAdapter())

            it.setOnInfoWindowClickListener(this)

            vm.initializeMapView(it)
            vm.addMarker(it)
        }
    }

    override fun onInfoWindowClick(p0: Marker?) {
        Log.d(TAG, "Restaurant ${p0?.title} clicked")
        p0 ?: return
        restaurantClick(p0)
    }

    internal inner class CustomInfoWindowAdapter : GoogleMap.InfoWindowAdapter {

        private val window: View = layoutInflater.inflate(R.layout.info_window, null)


        override fun getInfoContents(p0: Marker): View? {
            return null
        }

        override fun getInfoWindow(p0: Marker): View {
            render(p0, window)
            return window
        }


        private fun render(marker: Marker, view: View) {
            val titleUi = view.findViewById<TextView>(R.id.restaurantName)

            titleUi.text = marker.title
        }

    }
}