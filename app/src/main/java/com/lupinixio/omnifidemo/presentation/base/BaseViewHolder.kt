package com.lupinixio.omnifidemo.presentation.base

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T, B : ViewDataBinding>(
    protected val binding: B
) :
    RecyclerView.ViewHolder(binding.root) {

    protected var holderItem: T? = null

    open fun bind(obj: T) {
        holderItem = obj
//        binding.setVariable(BR.item, obj)
//        binding.executePendingBindings()
    }


}